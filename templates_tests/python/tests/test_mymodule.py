"""
I can even make docstrings here! I'm awesome.
"""

import logging

from mypackage import mymodule

logger = logging.getLogger(__name__)


def test_myfunction():
    """Testing always makes me joyful."""
    logger.debug("We're in the function now, amazing! This is a debug message.")
    logger.info("You should know about this INFOrmational message!")
    logger.warning("Be warned, things may get hairy now.")
    logger.error("Ah, an error, we screwed up.")
    logger.critical("Lord o lord, it's critical. But we must move on.")
    assert mymodule.myfunction(variable=3) == 11
