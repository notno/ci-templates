# Contributing

Would you like to contribute? There are 2 ways:
1. Fork this project, commit your changes there, and do a Merge Request against this project. Don't forget to assign either @falconierif or @hoverht to have a look at your code.
2. Ask us to become a Developer in this repo, so you can develop in a branch here.

### pre-commit hooks
We use pre-commit hooks in this project.
You don't have to use them, but if you want you can install them this way:
```bash
pip3 install poetry
poetry install
poetry run pre-commit install
poetry run pre-commit install --hook-type commit-msg
poetry run pre-commit install --hook-type post-commit
```
