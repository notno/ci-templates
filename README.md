# DEPRECATED

## Please use our new templates: https://gitlab.com/just-ci/templates

### These are Harder, Better, Faster, and most of all, Stronger

# CI templates

Maintainers:
* Federico Falconieri: @falconierif
* Ruben ten Hove: @hoverht

Any questions, problems, suggestions? [Create an issue](issues/new)!

## Version 3 released :tada: 

This repository contains a collection of modular gitlab ci jobs and pipelines.
In order to use them in your project, you will need to have a [GitLab runner](https://docs.gitlab.com/ee/ci/runners/) configured.

This is a example of how you import the pylint job from this repository in your 
own project's `.gitlab-ci.yml`:

```yaml
include:
  - project: 'ci/templates'
    file: 'python/pylint.yml'
```

That's all!

> See the [gitlab docs](https://docs.gitlab.com/ee/ci/yaml/includes.html) for more details on how
> includes work.

## Semantic versioning and breaking changes

We tag changes automagically using [semantic-release](https://semantic-release.gitbook.io/semantic-release/). Every time a merge request is accepted, a semantic-release job produces arelease (and an associated tag).

You can import jobs and pipelines from a specific tag or branch if you desire, for example if you
like the way things were before.
If you do not specify a tag or branch, you will import what is on `master`, i.e. the latest.
If your project does not work anymore because there has been a breaking change in `master`,
use an older tag like this:

```yaml
include:
  - project: 'ci/templates'
    file: 'python/pylint.yml'
    ref: 'v3.1.2'
```

## Customize kaniko builds

Kaniko is a special job here, as it builds Docker images for you without any special requirements,
but also prepares Docker images for specific tests which require an image.

You can add variables to customize the Kaniko build. Use the following syntax to setup
a kaniko build job, and change variables where you'd like. See comments for their use.
None are required, you can just just the first 3 lines and get your builds done.

```yaml
include:
  - project: 'ci/templates'
    file: 'docker/kaniko.yml'

docker:kaniko:
  variables:
    # Set to "false" to disable kaniko caching. See here: https://github.com/GoogleContainerTools/kaniko/blob/master/README.md#caching
    USE_CACHE: "true"
    # Will override all default destination with your custom tag
    DESTINATIONS: "--destination $CI_REGISTRY_IMAGE:my_awesome_tag"
    # Use these to give extra args to the kaniko executor. See here: https://github.com/GoogleContainerTools/kaniko/blob/master/README.md#additional-flags  
    EXTRA_ARGS: "--build-arg=ARG=VALUE"
    # Will add a $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA tag to your build, regardless of overriding $DESTINATIONS.
    DEV_BUILD: "true"
    DOCKERFILE: "mydockerfiles/myawesome.Dockerfile"  # Optional, defaults to just Dockerfile

another-build:
  extends: docker:kaniko  # This will inherit the above as well
  variables:
    DESTINATIONS: "--destination $CI_REGISTRY_IMAGE:my_custom_tag"
    DOCKERFILE: "mydockerfiles/mycustom.Dockerfile"
```

The example above now has two jobs, one is called `kaniko`, the other `another-build`.

## Python out-of-the-box pipelines

Do you want to use a basic set of jobs from this repository, but can't be bothered to 
import them one by one manually?
You can easily import one of three python pipelines with a focus on levels of quality like this:

#### Basic
```yaml
include:
  - project: 'ci/templates'
    file: 'pipelines/python-basic.yml'
```

#### Advanced
```yaml
include:
  - project: 'ci/templates'
    file: 'pipelines/python-docker-basic.yml'
```

#### Pro
```yaml
include:
  - project: 'ci/templates'
    file: 'pipelines/python-docker-pro.yml'
```

# Contributing

See [`CONTRIBUTING.md`](CONTRIBUTING.md)

# We even do drawio

![](/../-/jobs/artifacts/master/raw/templates_tests/other/drawio.png?job=docs%3Adrawio)
